package ku.csi.cs.appointment;
import static org.junit.Assert.assertEquals;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.Scanner;

import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

/** Chaiwat Watcharajindasakul 5610450098 */

public class AppointmentConsole {
	private AppointmentBook book;
	private Scanner sc;
	private boolean status;
	private FrameAppointment frame;
	private AppointmentConnectionSQLite sqlite;
	private AppointmentReadWriteFile file;
	public static void main(String[] args){
		ApplicationContext bf = new ClassPathXmlApplicationContext("appointmentBook.xml");
		AppointmentConsole console = (AppointmentConsole) bf.getBean("appointmentConsole");
		System.out.println(console.toString());
	}
	
	public AppointmentConsole(){
		book = new AppointmentBook();
		sc = new Scanner(System.in);
		status = true;
		frame = new FrameAppointment();
		
		frame.ActionListenerAddAppointment(new AddAppiontmentActionListener());
		frame.ActionListenerAddTask(new AddTaskActionListener());
		frame.ActionListenerDisplayTask(new DisplayTaskActionListener());
		frame.ActionListenerSearch(new SearchActionListener());

		// AppointmentBook for Read file - Write file noted
		//file = new AppointmentReadWriteFile(filename);
		//this.book = file.readAppointmentFile();
		
		//AppointmentBook for SQLite
		sqlite = new AppointmentConnectionSQLite();
		this.book = sqlite.readSQLite();
		
		frame.setVisible(true);
		//run();
	}
	
	class AddAppiontmentActionListener implements ActionListener{
		@Override
		public void actionPerformed(ActionEvent e) {
			Calendar cal = frame.getCalApp();
			String des = frame.getDesApp();
			String type = frame.getTypeAppointment();
			// Facade Pattern
			writeAppointmentAndTaskFacade(type, cal, des);
		}
		
	}
	
	class AddTaskActionListener implements ActionListener{
		@Override
		public void actionPerformed(ActionEvent e) {
			Calendar cal = frame.getCalTask();
			String des = frame.getDesTask();
			// Facade Pattern
			writeAppointmentAndTaskFacade("Task", cal, des);
		}
		
	}
	// Facade Pattern
	public void writeAppointmentAndTaskFacade(String type,Calendar cal,String des){
		// Write appointment SqLite
		sqlite.writeAppointmentSQLite(type, cal, des);
		// Appointment Factory
		book.addAppointment(type,cal, des);
		// Write file to noted
		//file.writeAppointmentFile(type, cal, des);
	}
		
	class SearchActionListener implements ActionListener{
		@Override
		public void actionPerformed(ActionEvent e) {
			frame.setAreaSearch(book.search(frame.getSearch()));
		}
		
	}
	class DisplayTaskActionListener implements ActionListener{
		@Override
		public void actionPerformed(ActionEvent e) {
			frame.setAreaTask(book.allTask());
		}
		
	}
	
	public void run(){
		while(status){
			System.out.print("<<Action>>\nAction (Add, Search, Display Task) : ");
			String choose = sc.nextLine();
			if(choose.equals("Add"))
				add();
			else if(choose.equals("Search"))
				search();
			else
				displayTask();
		}
	}
	
	public void add(){
		System.out.print("<<Type>>\nType (Appointment(A), Task(T) : ");
		String add = sc.nextLine();
		if(add.equals("A")){
			System.out.print("<<Add Appointment>>\nChoose [OneTime(1), Daily(2), Weekly(3), Monthly(4)] : ");
			String n = sc.nextLine();
			int i = Integer.parseInt(n);
			System.out.print("Date(ex. 15/04/2015) : ");
			String date = sc.nextLine();
			String[] cut1 = date.split("/");
			int day = Integer.parseInt(cut1[0]);
			int month = Integer.parseInt(cut1[1])-1;
			int year = Integer.parseInt(cut1[2]);
			System.out.print("Time(ex. 12:30) : ");
			String time = sc.nextLine();
			String[] cut2 = time.split(":");
			int hour = Integer.parseInt(cut2[0]);
			int min = Integer.parseInt(cut2[1]);
			Calendar cal = new GregorianCalendar(year,month,day,hour,min);
			System.out.print("Description : ");
			String des = sc.nextLine();
			if(i==1){ book.addAppointment(new Onetime(cal, des));}
			else if(i==2){ book.addAppointment(new Daily(cal, des));}
			else if(i==3){ book.addAppointment(new Weekly(cal, des));}
			else if(i==4){ book.addAppointment(new Monthly(cal, des));}
		}
		else{
			System.out.print("<<Add Task>>\nDate(ex. 15/04/2015) : ");
			String date = sc.nextLine();
			String[] cut1 = date.split("/");
			int day = Integer.parseInt(cut1[0]);
			int month = Integer.parseInt(cut1[1])-1;
			int year = Integer.parseInt(cut1[2]);
			System.out.print("Description : ");
			String des = sc.nextLine();
			Calendar cal = new GregorianCalendar(year,month,day);
			book.addTask(new Task(cal,des));
		}
	}
	
	public void search(){
		System.out.print("<<Search>>\nDate(ex. 15/04/2015) : ");
		String date = sc.nextLine();
		String[] cut1 = date.split("/");
		int day = Integer.parseInt(cut1[0]);
		int month = Integer.parseInt(cut1[1])-1;
		int year = Integer.parseInt(cut1[2]);
		Calendar cal = new GregorianCalendar(year,month,day);
		book.search(cal);
	}
	
	public void displayTask(){
		System.out.println("<<Display Task>>");
		System.out.println(book.allTask());
	}
	public String toString(){
		return book.toString();
	}
}
