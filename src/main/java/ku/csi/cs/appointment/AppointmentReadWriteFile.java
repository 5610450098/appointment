package ku.csi.cs.appointment;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.Calendar;
import java.util.GregorianCalendar;
/** Chaiwat Watcharajindasakul 5610450098 */
public class AppointmentReadWriteFile {
	private String filename;
	public AppointmentReadWriteFile(String name){
		filename = name;
	}
	public AppointmentBook readAppointmentFile(){
		AppointmentBook book = new AppointmentBook();
		try{
			FileReader fileReader = new FileReader(filename);
			BufferedReader buffer = new BufferedReader(fileReader);
			
			for(String line=buffer.readLine();line!=null;line=buffer.readLine()){
				String[] data = line.split(",");
				String type = data[0].trim();
				int year = Integer.parseInt(data[1].trim());
				int month = Integer.parseInt(data[2].trim())-1;
				int day = Integer.parseInt(data[3].trim());
				int hour = Integer.parseInt(data[4].trim());
				int min = Integer.parseInt(data[5].trim());
				String des = data[6].trim();
				Calendar cal = new GregorianCalendar(year,month,day,hour,min);
				// Appointment Factory
				book.addAppointment(type,cal, des);
			}
		}
		catch(FileNotFoundException e){
			System.err.println("Cannot read file "+filename);
		}
		catch(IOException e){
			System.err.println("Error reading from file");
		}
		return book;
	}
	public void writeAppointmentFile(String type,Calendar cal,String des){
		FileWriter fileWriter = null;
		try{
			fileWriter = new FileWriter(filename,true);
			PrintWriter out = new PrintWriter(fileWriter);
			int year = cal.get(Calendar.YEAR);
			int month = cal.get(Calendar.MONTH)+1;
			int day = cal.get(Calendar.DAY_OF_MONTH);
			int hour = cal.get(Calendar.HOUR_OF_DAY);
			int min = cal.get(Calendar.MINUTE);
			out.println(type+" ,"+year+" ,"+month+" ,"+day+" ,"+hour+" ,"+min+" ,"+des);
		}
		catch(IOException e){
			System.err.println("");
		}
		finally{
			try{
				if(fileWriter != null)
					fileWriter.close();
			}
			catch(IOException e){
				System.err.println("Error closing file");
			}
		}
	}
}
