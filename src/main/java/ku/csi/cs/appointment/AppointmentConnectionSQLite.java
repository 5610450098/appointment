package ku.csi.cs.appointment;

import java.sql.Connection;
import java.sql.DatabaseMetaData;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.Calendar;
import java.util.GregorianCalendar;
/** Chaiwat Watcharajindasakul 5610450098 */
public class AppointmentConnectionSQLite {
	private Connection conn;
	private Statement statement;
	public AppointmentConnectionSQLite(){
		try{
			Class.forName("org.sqlite.JDBC");
	 		String dbURL = "jdbc:sqlite:appointmentbook.db";
	 		conn = DriverManager.getConnection(dbURL);
	 		statement = conn.createStatement();
		}catch (ClassNotFoundException ex) {
	 		ex.printStackTrace();
	 	}catch (SQLException ex) {
	 		ex.printStackTrace();
	 	}
	}
	public AppointmentBook readSQLite(){
		AppointmentBook book = new AppointmentBook();
		try{ 
	 		if (conn != null) {
	 			String query = "Select * from appointment";
	 			ResultSet resultSet = statement.executeQuery(query);
	 			while (resultSet.next()) {
	 				String type = resultSet.getString(1);
	 				int year = Integer.parseInt(resultSet.getString(2));
	 				int month = Integer.parseInt(resultSet.getString(3));
	 				int day = Integer.parseInt(resultSet.getString(4));
	 				int hour = Integer.parseInt(resultSet.getString(5));
	 				int min = Integer.parseInt(resultSet.getString(6));
	 				String des = resultSet.getString(7);
	 				Calendar cal = new GregorianCalendar(year,month,day,hour,min);
	 				book.addAppointment(type,cal, des);	
	 			}
	 			String query2 = "Select * from task";
	 			ResultSet resultSet2 = statement.executeQuery(query2);
	 			while (resultSet2.next()) {
	 				int year = Integer.parseInt(resultSet.getString(1));
	 				int month = Integer.parseInt(resultSet.getString(2));
	 				int day = Integer.parseInt(resultSet.getString(3));
	 				String des = resultSet2.getString(4);
	 				Calendar cal = new GregorianCalendar(year,month,day);
	 				book.addTask(new Task(cal,des));
	 			}
	 			//System.out.println(book.toString());
			// conn.close();
	 		}
	 	}catch (SQLException ex) {
	 		ex.printStackTrace();
	 	}
		return book;
	}
	public void writeAppointmentSQLite(String type,Calendar cal,String des){
		try{
			int year = cal.get(Calendar.YEAR);
			int month = cal.get(Calendar.MONTH)+1;
			int day = cal.get(Calendar.DAY_OF_MONTH);
			int hour = cal.get(Calendar.HOUR_OF_DAY);
			int min = cal.get(Calendar.MINUTE);
			if(type.equals("Task")){
				 String sql = "INSERT INTO task (year,month,day,des) " +
		                 "VALUES ("+year+","+month+","+day+",'"+des+"' );"; 
				 statement.executeUpdate(sql);
			}
			else{
				String sql = "INSERT INTO appointment (type,year,month,day,hour,min,des) " +
		                 "VALUES ('"+type+"',"+year+","+month+","+day+","+hour+","+min+",'"+des+"' );"; 
				statement.executeUpdate(sql);
			}
			
		}catch (Exception ex) {
	 		ex.printStackTrace();
	 	}
	}
}
