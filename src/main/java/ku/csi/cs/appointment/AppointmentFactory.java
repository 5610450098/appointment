package ku.csi.cs.appointment;

import java.util.Calendar;
/** Chaiwat Watcharajindasakul 5610450098 */
public class AppointmentFactory {
	public Appointment getAppointment(String type,Calendar date,String description){
		if("Onetime".equals(type))
			return new Onetime(date, description);
		else if("Weekly".equals(type))
			return new Weekly(date, description);
		else if("Daily".equals(type))
			return new Daily(date, description);
		else if("Monthly".equals(type))
			return new Monthly(date, description);
		else if("Task".equals(type))
			return new Task(date, description);
		return null;
	}
}
