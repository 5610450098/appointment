package ku.csi.cs.appointment;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
/** Chaiwat Watcharajindasakul 5610450098 */
public class Task extends Appointment{
	private String description;
	private Calendar date;
	private boolean status;
	
	public Task(Calendar date,String description){
		super(date,description);
		status = false;
	}
	public void setStatus(){
		status = true;
	}
	public String getDate(){
		DateFormat df =  new SimpleDateFormat("dd/MM/yyyy");
		return df.format(super.getCalendar().getTime());
	}
	public boolean getStatus(){
		return status;
	}
	public String toString(){
		return "Task : date="+getDate()+", description="+super.getDescription()+"\tDone="+status;
	}
}
