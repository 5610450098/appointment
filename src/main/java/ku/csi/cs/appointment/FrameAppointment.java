package ku.csi.cs.appointment;

import java.awt.BorderLayout;
import java.awt.CardLayout;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.Calendar;
import java.util.GregorianCalendar;

import javax.swing.ButtonGroup;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JRadioButton;
import javax.swing.JScrollBar;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import javax.swing.JTextField;
import javax.swing.border.EtchedBorder;
import javax.swing.border.TitledBorder;
/** Chaiwat Watcharajindasakul 5610450098 */

public class FrameAppointment extends JFrame {
	private JButton app , task , search , display,confirmSearch,confirmApp,confirmTask;
	private JPanel panel,cardPanel;
	private CardLayout card;
	private JTextField desField1,desField2;
	private JRadioButton app1,app2,app3,app4;
	private String filename;
	private JTextArea areaTask,areaSearch;
	private JComboBox dateCombo1,monthCombo1,yearCombo1,hourComboBox,minComboBox,dateCombo2,monthCombo2,yearCombo2,dateCombo3,monthCombo3,yearCombo3;
	private JScrollPane scrollTask,scrollSearch;
	String[] dateList = new String[] {"01","02","03","04","05","06","07","08","09","10","11","12","13","14",
			"15","16","17","18","19","20","21","22","23","24","25","26","27","28","29","30","31"};
	String[] monthList = new String[] {"01","02","03","04","05","06","07","08","09","10","11","12"};
	String[] yearList = new String[] {"2015","2016","2017"};
	String[] hourList = new String[] {"00","01","02","03","04","05","06","07","08","09","10","11","12","13","14",
		"15","16","17","18","19","20","21","22","23"};
	String[] minList = new String[] {"00","01","02","03","04","05","06","07","08","09","10","11","12","13","14",
			"15","16","17","18","19","20","21","22","23","24","25","26","27","28","29","30","31","32","33","34","35","36","37","38","39",
				"40","41","42","43","44","45","46","47","48","49","50","51","52","53","54","55","56","57","58","59"};
	
	public FrameAppointment(){
		setTitle("APPOINTMENT");
		filename = "appointment.txt";
		card = new CardLayout();
		cardPanel = new JPanel();
		cardPanel.setLayout(card);
		setBounds(350,100,500,300);
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		getContentPane().setLayout(new BorderLayout(0,0));
		createFrame();
	}

	public void createFrame(){
		panel = new JPanel();
		getContentPane().add(panel,BorderLayout.NORTH);
		app = new JButton("Add Appointment");
		task = new JButton("Add Task");
		search = new JButton("Search");
		display = new JButton("Display Task");
		panel.add(app);
		panel.add(task);
		panel.add(search);
		panel.add(display);
		
		createAddAppointment();
		createAddTask();
		createSearch();
		createShowTask();
		add(cardPanel,BorderLayout.CENTER);
		
		app.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				card.show(cardPanel,"Add Appointment");
			}
		});
		task.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				card.show(cardPanel,"Add Task");
			}
		});
		search.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				card.show(cardPanel,"Search");
			}
		});
		display.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				card.show(cardPanel,"Display Task");
			}
		});
		//setVisible(true);
	}
	public JPanel createAddAppointment(){
		JPanel panel = new JPanel();
		panel.setLayout(new GridLayout(4,1));
		
		panel.setBorder(new TitledBorder(new EtchedBorder(),"Add Appointment"));
		app1 = new JRadioButton("One time");
		app2 = new JRadioButton("Daily");
		app3 = new JRadioButton("Weekly");
		app4 = new JRadioButton("Monthly");
		ButtonGroup group = new ButtonGroup();
		group.add(app1);
		group.add(app2);
		group.add(app3);
		group.add(app4);
		app1.setSelected(true);
		JPanel panelGroup = new JPanel();
		panelGroup.add(app1);
		panelGroup.add(app2);
		panelGroup.add(app3);
		panelGroup.add(app4);
		
		JPanel choosedate = new JPanel();
		JLabel label1 = new JLabel("Choose");
		JLabel label2 = new JLabel("Date");
		JLabel label3 = new JLabel("Month");
		JLabel label4 = new JLabel("Year");
		dateCombo1 = new JComboBox(dateList);
		monthCombo1 = new JComboBox(monthList);
		yearCombo1 = new JComboBox(yearList);
		choosedate.add(label1);
		choosedate.add(label2);
		choosedate.add(dateCombo1);
		choosedate.add(label3);
		choosedate.add(monthCombo1);
		choosedate.add(label4);
		choosedate.add(yearCombo1);
		
		JLabel time = new JLabel("Time");
		hourComboBox = new JComboBox(hourList);
		minComboBox = new JComboBox(minList);
		choosedate.add(time);
		choosedate.add(hourComboBox);
		choosedate.add(minComboBox);
		
		JPanel desPanel = new JPanel();
		JLabel desLabel = new JLabel("Description");
		desField1 = new JTextField(20);
		desPanel.add(desLabel);
		desPanel.add(desField1);
		JPanel buttonPanel = new JPanel();
		confirmApp = new JButton("Confirm");
		buttonPanel.add(confirmApp);
		panel.add(panelGroup);
		panel.add(choosedate);
		panel.add(desPanel);
		panel.add(buttonPanel);
		cardPanel.add(panel,"Add Appointment");
		return panel;
	}
	
	public JPanel createAddTask(){
		JPanel panel = new JPanel();
		panel.setLayout(new GridLayout(3,1));
		panel.setBorder(new TitledBorder(new EtchedBorder(),"Add Task"));
		JPanel choosedate = new JPanel();
		JLabel label1 = new JLabel("Choose");
		JLabel label2 = new JLabel("Date");
		JLabel label3 = new JLabel("Month");
		JLabel label4 = new JLabel("Year");
		dateCombo2 = new JComboBox(dateList);
		monthCombo2 = new JComboBox(monthList);
		yearCombo2 = new JComboBox(yearList);
		choosedate.add(label1);
		choosedate.add(label2);
		choosedate.add(dateCombo2);
		choosedate.add(label3);
		choosedate.add(monthCombo2);
		choosedate.add(label4);
		choosedate.add(yearCombo2);
		JPanel desPanel = new JPanel();
		JLabel desLabel = new JLabel("Description");
		desField2 = new JTextField(20);
		desPanel.add(desLabel);
		desPanel.add(desField2);
		JPanel buttonPanel = new JPanel();
		confirmTask = new JButton("Confirm");
		buttonPanel.add(confirmTask);
		panel.add(choosedate);
		panel.add(desPanel);
		panel.add(buttonPanel);
		cardPanel.add(panel,"Add Task");
		return panel;
	}
	
	public JPanel createSearch(){
		JPanel panel = new JPanel();
		panel.setBorder(new TitledBorder(new EtchedBorder(),"Search"));
		JPanel panel2 = new JPanel();
		JPanel choosedate = new JPanel();
		JLabel label1 = new JLabel("Choose");
		JLabel label2 = new JLabel("Date");
		JLabel label3 = new JLabel("Month");
		JLabel label4 = new JLabel("Year");
		dateCombo3 = new JComboBox(dateList);
		monthCombo3 = new JComboBox(monthList);
		yearCombo3 = new JComboBox(yearList);
		choosedate.add(label1);
		choosedate.add(label2);
		choosedate.add(dateCombo3);
		choosedate.add(label3);
		choosedate.add(monthCombo3);
		choosedate.add(label4);
		choosedate.add(yearCombo3);
		areaSearch = new JTextArea();
		scrollSearch = new JScrollPane(areaSearch);
		confirmSearch = new JButton("Confirm");
		panel2.add(choosedate);
		panel2.add(confirmSearch);
		panel.add(panel2);
		panel.add(scrollSearch);
		panel2.setBounds(20,20,450,50);
		scrollSearch.setBounds(20,70,450,140);
		panel.setLayout(null);
		cardPanel.add(panel,"Search");
		return panel;
	}
	public JPanel createShowTask(){
		JPanel panel = new JPanel();
		panel.setBorder(new TitledBorder(new EtchedBorder(),"Display Task"));
		areaTask = new JTextArea("");
		scrollTask = new JScrollPane(areaTask);
		panel.add(scrollTask);
		scrollTask.setBounds(20,20,440,190);
		panel.setLayout(null);
		cardPanel.add(panel,"Display Task");
		return panel;
	}
	public void ActionListenerAddAppointment(ActionListener list){
		confirmApp.addActionListener(list);
	}
	public void ActionListenerAddTask(ActionListener list){
		confirmTask.addActionListener(list);
	}
	public void ActionListenerDisplayTask(ActionListener list){
		display.addActionListener(list);
	}
	public void ActionListenerSearch(ActionListener list){
		confirmSearch.addActionListener(list);
	}
	public void setAreaTask(String s){
		areaTask.setText(s);
	}
	public Calendar getSearch(){
		return new GregorianCalendar(Integer.parseInt(yearCombo3.getSelectedItem()+""),Integer.parseInt(monthCombo3.getSelectedItem()+"")-1,Integer.parseInt(dateCombo3.getSelectedItem()+""));
	}
	public void setAreaSearch(String s){
		areaSearch.setText(s);
	}
	public void clearSarch(){
		dateCombo3.setSelectedIndex(0);
		monthCombo3.setSelectedIndex(0);
		yearCombo3.setSelectedIndex(0);
	}
	public Calendar getCalTask(){
		return new GregorianCalendar(Integer.parseInt(yearCombo2.getSelectedItem()+""),Integer.parseInt(monthCombo2.getSelectedItem()+"")-1,Integer.parseInt(dateCombo2.getSelectedItem()+""));
	}
	public String getDesTask(){
		return desField2.getText();
	}
	public void claerAddTask(){
		desField2.setText("");
		dateCombo2.setSelectedIndex(0);
		monthCombo2.setSelectedIndex(0);
		yearCombo2.setSelectedIndex(0);
	}
	public Calendar getCalApp(){
		return new GregorianCalendar(Integer.parseInt(yearCombo1.getSelectedItem()+""),Integer.parseInt(monthCombo1.getSelectedItem()+"")-1,Integer.parseInt(dateCombo1.getSelectedItem()+""),Integer.parseInt((String)hourComboBox.getSelectedItem()),Integer.parseInt((String)minComboBox.getSelectedItem()));
	}
	public String getDesApp(){
		return desField1.getText();
	}
	
	public String getTypeAppointment(){
		if(app1.isSelected())
			return "Onetime";
		else if(app2.isSelected())
			return "Daily";
		else if(app3.isSelected())
			return "Weekly";
		else if(app4.isSelected())
			return "Monthly";
		return "";
	}
	public void clearAddApp(){
		app1.setSelected(true);
		desField1.setText("");
		dateCombo1.setSelectedIndex(0);
		monthCombo1.setSelectedIndex(0);
		yearCombo1.setSelectedIndex(0);
		hourComboBox.setSelectedIndex(0);
		minComboBox.setSelectedIndex(0);
	}
	
}
