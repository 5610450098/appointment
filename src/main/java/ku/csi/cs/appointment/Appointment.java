package ku.csi.cs.appointment;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

/** Chaiwat Watcharajindasakul 5610450098 */

public class Appointment {
	private String description,d;
	private Calendar date;
	
	public Appointment(Calendar date,String description){
		this.date = date;
		this.description = description;
	}
	public Appointment(CalendarAppointment date,String description){
		this.date = date.getCalendar();
		this.description = description;
	}
	public Calendar getCalendar(){
		return date;
	}
	public String getDate(){
		DateFormat df =  new SimpleDateFormat("dd/MM/yyyy HH:mm");
		return df.format(date.getTime());
	}
	public String getDescription(){
		return description;
	}
	public String toString(){
		return "Appointment : date="+getDate()+", description="+description;
	}
}
