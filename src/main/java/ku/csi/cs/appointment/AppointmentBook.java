package ku.csi.cs.appointment;
import java.text.DateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.List;

/** Chaiwat Watcharajindasakul 5610450098 */

public class AppointmentBook {
	 private ArrayList<Appointment> bookList;
	 private ArrayList<Appointment> taskList;
	 private AppointmentFactory appointFac;
	 public AppointmentBook(){
		 bookList = new ArrayList<Appointment>();
		 taskList = new ArrayList<Appointment>();
		 appointFac = new AppointmentFactory();
	 }
	 
	 // Appointment Factory
	 public void addAppointment(String type,Calendar date,String description){
		 if(type.equals("Task")){
			 taskList.add(appointFac.getAppointment(type, date, description));
		 }
		 bookList.add(appointFac.getAppointment(type, date, description));
	 }
	 public void addAppointment(Appointment app){
		 bookList.add(app);
	 }
	 public void addTask(Task task){
		 taskList.add(task);
	 }
	 public void addAll(List<Appointment> list){
		 for(Appointment app : list){
			 bookList.add(app);
		 }
	 }
	 
	 public String toString(){
		 checkTask();
		 String str = "";
		 for(Appointment a:bookList){
			 str+=a+"\n";
		 }
		 for(Appointment t:taskList){
			 str+=t+"\n";
		 }
		 return str;
	 }
	 public String allTask(){
		 checkTask();
		 String str = "";
		 for(Appointment t:taskList){
			 str+=t+"\n";
		 }
		 return str;
	 }
	 
	 public int getNumAppointments(){
		 return bookList.size();
	 }

	 public void checkTask(){
		 for(Appointment task:taskList){
			 String des = task.getDescription();
			 Calendar cal = task.getCalendar();
			 int year = cal.get(Calendar.YEAR);
			 int month = cal.get(Calendar.MONTH);
			 int day = cal.get(Calendar.DAY_OF_MONTH);
			 for(Appointment app:bookList){
				 String desc = app.getDescription();
				 Calendar date = app.getCalendar();
				 int y = date.get(Calendar.YEAR);
				 int m = date.get(Calendar.MONTH);
				 int d = date.get(Calendar.DAY_OF_MONTH);
				 if(y==year && m==month && d==day && des.equals(desc)){
					 Task tasks = (Task) task;
					 tasks.setStatus();
				 }
			 }
		 }
	 }
	 public ArrayList<Appointment> getAppointmentList(){
		 return bookList;
	 }
	 public ArrayList<Appointment> getTaskList(){
		 return taskList;
	 }
	 public String search(Calendar cal){
		 checkTask();
		 String str = "";
		 int year = cal.get(Calendar.YEAR);
		 int month = cal.get(Calendar.MONTH);
		 int day = cal.get(Calendar.DAY_OF_MONTH);
		 for(Appointment app:bookList){
			 Calendar date = app.getCalendar();
			 int y = date.get(Calendar.YEAR);
			 int m = date.get(Calendar.MONTH);
			 int d = date.get(Calendar.DAY_OF_MONTH);
			 int hour = date.get(Calendar.HOUR_OF_DAY);
			 int min = date.get(Calendar.MINUTE);
			 if(app instanceof Daily && day>=d && month>=m && year>=y){
				 str += new Daily(new GregorianCalendar(year,month,day,hour,min),app.getDescription())+"\n";
			 }
			 else if(app instanceof Monthly && day>=d && month>=m && year>=y){
				 if(day==d){
					 str += new Monthly(new GregorianCalendar(year,month,day,hour,min), app.getDescription())+"\n";
				 }
			 }
			 else if(app instanceof Weekly && day>=d && month>=m && year>=y){
				 if(cal.get(Calendar.DAY_OF_WEEK)==date.get(Calendar.DAY_OF_WEEK)){
					 str += new Weekly(new GregorianCalendar(year,month,day,hour,min), app.getDescription()+"\n");
				 }
			 }
			 else{
				 if(y==year && m==month && d==day){
						str += app+"\n";
					}
			 } 
		 }
		 for(Appointment task:taskList){
			 Calendar date = task.getCalendar();
			 int y = date.get(Calendar.YEAR);
			 int m = date.get(Calendar.MONTH);
			 int d = date.get(Calendar.DAY_OF_MONTH);
			 if(y==year && m==month && d==day){
				 str+=task+"\n";
			 }
		 }
		 return str;
	 }
}