package ku.csi.cs.appointment;

import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

public class MainAppointmentXML {
	public static void main(String[] args){
		ApplicationContext bf = new ClassPathXmlApplicationContext("appointment.xml");
		Appointment appointment = (Appointment) bf.getBean("appointment");
		System.out.println("Test Appointment"+appointment.toString());
	}
}
