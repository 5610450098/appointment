package ku.csi.cs.appointment;

import java.util.Calendar;
import java.util.GregorianCalendar;

public class CalendarAppointment {
	private int year,month,date,min,hour;
	
	public CalendarAppointment(int y,int m,int d,int hour,int min){
		year = y;
		month = m;
		date = d;
		this.min = min;
		this.hour = hour;
	}
	public Calendar getCalendar(){
		return new GregorianCalendar(year,month,date,hour,min);
	}
}
