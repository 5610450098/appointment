package ku.csi.cs.appointment;


import java.util.Calendar;
import java.util.Date;
/** Chaiwat Watcharajindasakul 5610450098 */
public class Onetime extends Appointment{

	public Onetime(Calendar date, String description) {
		super(date, description);
	}
	public String toString(){
		return "Onetime : date="+getDate()+", description="+getDescription();
	}
}
