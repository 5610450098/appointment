package ku.csi.cs.appointment;

import static org.junit.Assert.*;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;

import org.junit.Before;
import org.junit.Test;
/** Chaiwat Watcharajindasakul 5610450098 */
public class AppointmentTest {
	private AppointmentBook book;
	private AppointmentFactory fac;
	@Before
	public void appointmentBefore(){
		book = new AppointmentBook();
		book.addAppointment(new Weekly(new GregorianCalendar(2015,9,13,10,30), "Lab SE"));
		book.addAppointment(new Daily(new GregorianCalendar(2015,8,13,12,00), "Lunch with my friend"));
		book.addTask(new Task(new GregorianCalendar(2015,9,13), "Meeting"));
		book.addTask(new Task(new GregorianCalendar(2015,10,23), "Go to Pattaya"));
		
		// Appointment Factory
		fac = new AppointmentFactory();
	}
	@Test
	public void testShowTask(){
		AppointmentBook book = new AppointmentBook();
		book.addTask(new Task(new GregorianCalendar(2015,9,13), "Meeting"));
		book.addTask(new Task(new GregorianCalendar(2015,10,23), "Go to Pattaya"));
		assertEquals(book.allTask(),"Task : date=13/10/2558, description=Meeting\tDone=false\nTask : date=23/11/2558, description=Go to Pattaya\tDone=false\n");
	}
	
	@Test
	public void testSearchTaskFalse(){
		assertEquals(book.search(new GregorianCalendar(2015,8,13)),"Daily : date=13/09/2558 12:00, description=Lunch with my friend\n");
	}
	
	@Test
	public void testSearchTaskTrue(){
		book.addAppointment(new Onetime(new GregorianCalendar(2015,10,23,9,00), "Go to Pattaya"));
		assertEquals(book.search(new GregorianCalendar(2015,10,23)),"Daily : date=23/11/2558 12:00, description=Lunch with my friend\n"
				+ "Onetime : date=23/11/2558 09:00, description=Go to Pattaya\nTask : date=23/11/2558, description=Go to Pattaya\tDone=true\n");
	}
	
	@Test
	public void testNullFactory(){
		assertNull(fac.getAppointment("",new GregorianCalendar(2016,2,14),"My Love"));
	}
	
	@Test
	public void testDailyFactory(){
		assertEquals("Onetime : date=14/02/2559 00:00, description=My Love",fac.getAppointment("Onetime",new GregorianCalendar(2016,1,14),"My Love").toString());
	}

}
